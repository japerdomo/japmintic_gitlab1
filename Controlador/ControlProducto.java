/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Categoria;
import Modelo.Producto;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Jesús Perdomo
 */
public class ControlProducto {
     //ATRIBUTOS: 
    Producto objProducto = new Producto(); 
    Categoria objCategoria = new Categoria(); //NUEVO CATEGORIA
    
    //METODOS:
    public void crear(String nombre, String cantidad, 
                        int precio, Categoria categoria) {
        
        try{
            objProducto.setNombre(nombre);
            objProducto.setCantidad(Integer.parseInt(cantidad));
            objProducto.setPrecio(precio);
            objProducto.setCategoria(categoria);
            objProducto.crearProducto();
        }
        catch(Exception error){
            JOptionPane.showMessageDialog(null, "Error: "+error);
        }
    }
    
    
    public ArrayList listar(){
        try{
            ResultSet consulta = objProducto.listarProducto(); 
            ArrayList<Producto> listaProducto = new ArrayList<>(); 
            
            while(consulta.next()){
                objProducto = new Producto(); 
                objProducto.setId(consulta.getInt(1));
                objProducto.setNombre(consulta.getString(2));
                objProducto.setCantidad(consulta.getInt(3));
                objProducto.setPrecio(consulta.getInt(4));
                
                //Para Categoría inicio
                objCategoria = new Categoria();
                objCategoria.setIdCategoria(consulta.getInt(5));
		if (consulta.getInt(5)==1){
                    objCategoria.setNombreCategoria("Lacteos");
		}
		else if (consulta.getInt(5)==2){
                    objCategoria.setNombreCategoria("Abarrotes");
		}
                else if (consulta.getInt(5)==3){
                    objCategoria.setNombreCategoria("Carnicos");
		}
		objProducto.setCategoria(objCategoria);
                //Para Categoría Fin
                
                listaProducto.add(objProducto); 
            }
            
            return listaProducto; 
        }
        catch(Exception error){
            JOptionPane.showMessageDialog(null, "Error CtrlProd listar: "+error);
        }
        
        return null; 
    }
    
    public void actualizar(String id, String nombre, String cantidad, 
                        int precio) {
        
        try{
            objProducto.setId(Integer.parseInt(id));
            objProducto.setNombre(nombre);
            objProducto.setCantidad(Integer.parseInt(cantidad));
            objProducto.setPrecio(precio);
            objProducto.actualizarProducto(); 
        }
        catch(Exception error){
            JOptionPane.showMessageDialog(null, "Error: "+error);
        }
    }
    
    public void eliminar(String id) {
        
        try{
            objProducto.setId(Integer.parseInt(id));
            objProducto.eliminarProducto();
        }
        catch(Exception error){
            JOptionPane.showMessageDialog(null, "Error: "+error);
        }
    }
    
    public ArrayList consultar(String nombre){
        try{
            objProducto.setNombre(nombre);
            ResultSet consulta = objProducto.consultarProducto(); 
            ArrayList<Producto> consultaProducto = new ArrayList<>(); 
            
            while(consulta.next()){
                objProducto = new Producto(); 
                objProducto.setId(consulta.getInt(1));
                objProducto.setNombre(consulta.getString(2));
                objProducto.setCantidad(consulta.getInt(3));
                objProducto.setPrecio(consulta.getInt(4));
                
                //Para Categoría inicio
                objCategoria = new Categoria();
                objCategoria.setIdCategoria(consulta.getInt(5));
		if (consulta.getInt(5)==1){
                    objCategoria.setNombreCategoria("Lacteos");
		}
		else if (consulta.getInt(5)==2){
                    objCategoria.setNombreCategoria("Abarrotes");
		}
                else if (consulta.getInt(5)==3){
                    objCategoria.setNombreCategoria("Carnicos");
		}
		objProducto.setCategoria(objCategoria);
                //Para Categoría Fin
                
                consultaProducto.add(objProducto); 
            }
            
            return consultaProducto; 
        }
        catch(Exception error){
            JOptionPane.showMessageDialog(null, "Error: "+error);
        }
        
        return null; 
    }
   
}
